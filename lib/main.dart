import 'package:flutter/material.dart';
import 'package:word_info/pages/choose_location.dart';
import 'package:word_info/pages/country_info.dart';

void main() => runApp(
      MaterialApp(
        initialRoute: '/',
        routes: {
          '/info': (context) => CountryInfo(),
          '/': (context) => ChooseCountry()
        },
      ),
    );
