import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/map.jpeg'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Center(
          child: SpinKitWave(
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}
