import 'dart:convert';
import 'package:http/http.dart';

class Countries {
  final String apiURL =
      'https://restcountries.eu/rest/v2/all?fields=name;nativeName;alpha2Code;flag';

  Future<List> fetchData() async {
    Response response = await get(apiURL);
    return jsonDecode(response.body);
  }
}
