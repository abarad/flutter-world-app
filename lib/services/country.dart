import 'dart:convert';
import 'package:http/http.dart';

class Country {
  String name;
  String apiURL;
  Map data;

  String capital;
  String displayName;
  String domains;
  String population;
  String area;
  String currencies;
  String languages;
  String flag;
  String nativeName;

  Country(String name) {
    this.name = name;
    this.apiURL = 'https://restcountries.eu/rest/v2/name/$name';
  }

  String parseDomains(List topLevelDomain) {
    String parsedDomains = '';
    topLevelDomain.forEach((domain) => parsedDomains += ' $domain');
    return parsedDomains;
  }

  String parseCurrencies(List currencies) {
    String parsedCurrensies = '';
    currencies.forEach(
        (cur) => parsedCurrensies += ' ${cur["name"]} (${cur["symbol"]}) ');
    return parsedCurrensies;
  }

  String parseLanguages(List languages) {
    String parsedLanguages = '';
    languages.forEach((lang) =>
        parsedLanguages += ' ${lang["name"]} (${lang["nativeName"]}) ');
    return parsedLanguages;
  }

  Future<Country> fetchData() async {
    Response response = await get(apiURL);
    data = jsonDecode(response.body)[0];
    flag = 'images/flags/${data["alpha2Code"].toLowerCase()}.png';
    nativeName = data['nativeName'];
    displayName = '$name ($nativeName)';
    capital = data['capital'];
    domains = parseDomains(data['topLevelDomain']);
    population = data['population'].toString();
    area = data['area'].toString();
    currencies = parseCurrencies(data['currencies']);
    languages = parseLanguages(data['languages']);
    return this;
  }
}
