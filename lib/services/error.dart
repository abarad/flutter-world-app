import 'package:flutter/material.dart';

class GetError extends StatelessWidget {
  final Error error;
  GetError({this.error});
  @override
  Widget build(BuildContext context) {
    print(error.stackTrace);
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'ERROR',
            style: TextStyle(
              color: Colors.redAccent,
              fontSize: 48.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            error.runtimeType.toString(),
            style: TextStyle(
              color: Colors.redAccent,
              fontSize: 28.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
