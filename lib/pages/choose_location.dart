import 'package:flutter/material.dart';
import 'package:word_info/services/countries.dart';
import 'package:word_info/services/loader.dart';
import 'package:word_info/services/error.dart';
import 'package:word_info/pages/country_info.dart';

class ChooseCountry extends StatefulWidget {
  @override
  _ChooseCountryState createState() => _ChooseCountryState();
}

class _ChooseCountryState extends State<ChooseCountry> {
  final String code = 'alpha2Code';
  final String name = 'name';
  final String nativeName = 'nativeName';

  @override
  Widget build(BuildContext context) {
    ListTile getListTile(String country, String nativeName, String code) =>
        ListTile(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CountryInfo(name: country),
              ),
            );
          },
          contentPadding: EdgeInsets.symmetric(
            horizontal: 10.0,
            vertical: 5.0,
          ),
          leading: Container(
            padding: EdgeInsets.all(12.0),
            decoration: BoxDecoration(
              border: Border(
                right: BorderSide(
                  width: 1.0,
                  color: Colors.white24,
                ),
              ),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Image.asset(
                code,
                height: 70.0,
                width: 50.0,
                fit: BoxFit.cover,
              ),
            ),
          ),
          title: Text(
            country,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.normal,
              fontSize: 16.0,
            ),
          ),
          subtitle: Text(
            nativeName,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: Colors.white,
              fontSize: 12.0,
            ),
          ),
          trailing: Icon(
            Icons.keyboard_arrow_right,
            color: Colors.white,
            size: 30.0,
          ),
        );
    Card getCard(String country, String nativeName, String code) => Card(
          elevation: 8.0,
          margin: EdgeInsets.symmetric(
            horizontal: 10.0,
            vertical: 6.0,
          ),
          child: Container(
            decoration: BoxDecoration(
              color: Color.fromRGBO(64, 75, 96, .9),
            ),
            child: getListTile(
              country,
              nativeName,
              code,
            ),
          ),
        );

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/map.jpeg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SafeArea(
            child: FutureBuilder(
                future: Countries().fetchData(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        List data = snapshot.data;
                        String countryFlag =
                            '${data[index][code].toLowerCase()}.png';
                        String countryName = data[index][name];
                        String countryCapital = data[index][nativeName];
                        String countryCode = 'images/flags/$countryFlag';
                        return getCard(
                            countryName, countryCapital, countryCode);
                      },
                    );
                  } else if (snapshot.hasError) {
                    return GetError(error: snapshot.error);
                  } else {
                    return Loader();
                  }
                }),
          ),
        ],
      ),
    );
  }
}
