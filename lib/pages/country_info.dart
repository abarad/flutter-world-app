import 'package:word_info/services/loader.dart';
import 'package:flutter/material.dart';
import 'package:word_info/services/country.dart';
import 'package:word_info/services/error.dart';
import 'package:carousel_slider/carousel_slider.dart';

class CountryInfo extends StatelessWidget {
  final String name;
  CountryInfo({this.name});

  @override
  Widget build(BuildContext context) {
    Container getCarouselSlide({String name, String data}) {
      return Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(horizontal: 5.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              name,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 18.0,
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              data,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 24.0,
                color: Colors.white,
              ),
            ),
          ],
        ),
      );
    }

    Center getContent(Country country) {
      List<Map> facts = [
        {
          'name': 'Name',
          'data': country.displayName,
          'imagePath': 'images/cards/name.jpeg',
        },
        {
          'name': 'Capital',
          'data': country.capital,
          'imagePath': 'images/cards/capital.jpeg',
        },
        {
          'name': 'Domain',
          'data': country.domains,
          'imagePath': 'images/cards/domain.jpeg',
        },
        {
          'name': 'Population',
          'data': country.population,
          'imagePath': 'images/cards/population.jpeg',
        },
        {
          'name': 'Area',
          'data': country.area,
          'imagePath': 'images/cards/area.jpeg',
        },
        {
          'name': 'Currency',
          'data': country.currencies,
          'imagePath': 'images/cards/currency.jpeg',
        },
        {
          'name': 'Language',
          'data': country.languages,
          'imagePath': 'images/cards/language.jpeg',
        },
      ];
      return Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: MediaQuery.of(context).size.height * 0.12),
            ClipRRect(
              borderRadius: BorderRadius.circular(15.0),
              child: Image.asset(
                country.flag,
                height: MediaQuery.of(context).size.height * 0.3,
                width: MediaQuery.of(context).size.width * 0.8,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(height:  MediaQuery.of(context).size.height * 0.05),
            Padding(
              padding: EdgeInsets.all(10.0),
              child: CarouselSlider(
                height: MediaQuery.of(context).size.height * 0.4,
                items: facts.map(
                  (item) {
                    return Builder(
                      builder: (BuildContext context) {
                        return Stack(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15.0),
                                  color: Color.fromRGBO(58, 66, 86, 1.0),
                                ),
                              ),
                            ),
                            getCarouselSlide(
                              data: item['data'],
                              name: item['name'],
                            ),
                          ],
                        );
                      },
                    );
                  },
                ).toList(),
              ),
            ),
          ],
        ),
      );
    }

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/map.jpeg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          FutureBuilder(
            future: Country(name).fetchData(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return getContent(snapshot.data);
              } else if (snapshot.hasError) {
                return GetError(error: snapshot.error);
              } else {
                return Loader();
              }
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        child: Icon(
          Icons.edit,
          color: Colors.white,
          size: 30.0,
        ),
        onPressed: () {
          Navigator.pushNamed(context, '/');
        },
      ),
    );
  }
}
